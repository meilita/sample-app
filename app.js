const express = require('express');
const mongoose = require('mongoose');
const app = express();
const postcontroller = require('./controllers/post_controller');

//mongoDB connection string
const url = "mongodb+srv://meilita123:binar12345@sampledb-je6jo.mongodb.net/test?retryWrites=true&w=majority"

app.get('/', (req, res, next)=>{
    res.send('running node api');
});

const apiroutes = require('./routes/api_routes.js');

app.use('/', apiroutes); //using routes specified externally

mongoose.connect(url,{
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true
})
  .then(()=>{
    app.listen(3000);
    console.log('database connected!');})
  .catch(err => console.log(err));


  app.use(express.json()) //sets content-type to json
