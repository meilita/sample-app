exports.showIndex = (req, res, next) => {
    res.send('ruunning node api');
}

const Post = require('../models/post_model.js');//include post schema
exports.addPost = (req, res, next) => {
      const post = new Post({
           title: req.body.title,
           description: req.body.description,
           image: req.body.image
      }).then(result => {
           return result.save()
      }).then(() => {
           res.send('post added successfully');
      }).catch(err => {
           res.status(400).send(err);
      })
}